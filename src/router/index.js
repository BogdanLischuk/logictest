import { createRouter, createWebHashHistory } from 'vue-router'
import Notes from '../views/Notes.vue'
import Note from '@/views/Note'

const routes = [
  {
    path: '/',
    name: 'Notes',
    component: Notes
  },
  {
    path: '/note',
    name: 'Note',
    component: Note,
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  mode: 'history',
  routes,
})

export default router
