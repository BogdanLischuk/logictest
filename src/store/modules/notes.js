
export default ({
    
    namespaced: true,

    state: {
        notes: [
            {
                id: 1,
                name: 'Заметка 1',
                tasks: [
                    {   
                        id: 1,
                        name: 'Создать заметку',
                        isDone: false
                    }, 
                    {
                        id: 2,
                        name: 'Редагувати заметку',
                        isDone: true
                    }, 
                    {
                        id:3,
                        name: 'Удалить Заметку',
                        isDone: false
                    }
                ]
            },
            {
                id: 2,
                name: 'Test2',
                tasks: [
                    {   
                        id: 1,
                        name: 'Task1',
                        isDone: false
                    }, 
                    {
                        id: 2,
                        name: 'Task2',
                        isDone: true
                    }, 
                    {
                        id:3,
                        name: 'Task3',
                        isDone: false
                    }
                ]
            },
        ]
    },
    getters:{
        getNotes(state){
            return state.notes
        }
    },
    mutations: {
        editNote(state, note){
            let index = state.notes.findIndex(t => t.id == note.id)
            state.notes[index] = note
        },
        ADD_NOTE_MT(state, note) {
            note.id = state.notes.length + 1
            note.tasks = []
            state.notes.push(note)
        },
        DELETE_NOTE_MT(state, note){
            let index = state.notes.findIndex(t => t.id == note.id)
            state.notes.splice(index,1)
        },
        CHECK_TASK(state, params){
            let index = state.notes.findIndex(t => t.id == params.noteId)
            let taskIndex = state.notes[index].tasks.findIndex(t => t.id == params.taskId)
            state.notes[index].tasks[taskIndex].isDone = params.value
        },
        SAVE_EDITED_TASK(state, params){
            let index = state.notes.findIndex(t => t.id == params.noteId)
            let taskIndex = state.notes[index].tasks.findIndex(t => t.id == params.taskId)
            state.notes[index].tasks[taskIndex].name = params.value
        },
        DELETE_TASK(state, params){
            let index = state.notes.findIndex(t => t.id == params.noteId)
            let taskIndex = state.notes[index].tasks.findIndex(t => t.id == params.taskId)
            state.notes[index].tasks.splice(taskIndex, 1);
        },
        ADD_TASK(state, params){
            let index = state.notes.findIndex(t => t.id == params.noteId)
            state.notes[index].tasks.push(params.newTask)
        }
    },
    actions: {
        editNote(ctx, note){
            ctx.commit('editNote', note)
        },
        addNote(ctx, note){
            ctx.commit('ADD_NOTE_MT', note)
        },
        deleteNote(ctx, note){
            ctx.commit('DELETE_NOTE_MT', note)
        },
    },

})
