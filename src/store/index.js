import { createStore } from 'vuex'
import notes from '@/store/modules/notes'
import createPersistedState from "vuex-persistedstate";

export default createStore({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  getters:{

  },
  modules: {
    notes
  },
  plugins: [createPersistedState()],
})
 